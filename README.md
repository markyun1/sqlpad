# SQLPad CEElake develop 版本

## 快速上手开发

```sh
cd server
yarn

yarn start
```

In the other start the development server for the front end.

```sh
cd client
yarn

yarn start

# 如果没有数据库，下面可用于生成用于测试和本地开发的测试 SQLite 数据库. server: // 路径：server/generate-test-db-fixture.js
# Build test deb for test cases and to use during dev
node .\generate-test-db-fixture.js

```


启动后访问：

  http://localhost:3000 serves React-based frontend in dev-mode
  http://localhost:3010 serves frontend compiled for production


修改内容：

  1、修改了默认登录的路由文件
  <Route exact path="/signin" children={<AutoSignIn />} />
  2、修改了配置文件的默认密码
    config-example.env
  3、


编译发布：
  在 serves 执行 npm run  prepublishOnly

  or

  Windows 在根目录执行  ./scripts/build.sh

  ```sh
  scripts/build.sh
  ```

<!--  下面是官方 RD 文档 -->
A web app for writing and running SQL queries and visualizing the results. Supports Postgres, MySQL, SQL Server, ClickHouse, Crate, Vertica, Trino, Presto, SAP HANA, Cassandra, Google BigQuery, SQLite, TiDB and many more via [ODBC](https://github.com/sqlpad/sqlpad/wiki/ODBC).

![SQLPad Query Editor](https://user-images.githubusercontent.com/303966/99915755-32f78e80-2ccb-11eb-9f74-b18846d6108d.png)

## Project Status

SQLPad is a legacy project in maintenance mode. If evaluating SQLPad, please consider a [potential alternative](https://getsqlpad.com/en/introduction/#alternatives) or forking the project and making it your own.

Maintenance releases for security and dependency updates will continue as possible.

**As of version 7, semver is no longer followed**. Going forward patch updates may require major Node.js version updates, or contain removal of functionality.

## Docker Image

The docker image runs on port 3000 and uses `/var/lib/sqlpad` for the embedded database directory.

See [docker-examples](https://github.com/sqlpad/sqlpad/tree/master/docker-examples) for docker-compose examples.

## Project Documentation

Documentation located at [https://getsqlpad.com](https://getsqlpad.com).

## Development

For instructions on installing/running SQLPad from git repo see [DEVELOPER-GUIDE.md](https://github.com/sqlpad/sqlpad/blob/master/DEVELOPER-GUIDE.md)

## License

MIT
