import baseUrl from './baseUrl';
import { api } from './api';

function useAppContext() {
  let { data } = api.useAppInfo();

  const _data = {
    currentUser: {
      id: '1b23d8a4-dc9b-4f68-bf2a-1a91ebcd12f2',
      email: 'sqlpad@sqlpad.com',
      role: 'admin',
      name: 'CEE SqlPad',
      ldapId: null,
    },
    // 数据库 链接 信息
    config: {
      allowCsvDownload: true,
      baseUrl: '/sqlpad',
      defaultConnectionId: 'devdbdriverid123',
      editorWordWrap: false,
      googleAuthConfigured: false,
      localAuthConfigured: true,
      publicUrl: '',
      samlConfigured: false,
      samlLinkHtml: 'Sign in with SSO',
      ldapConfigured: false,
      ldapRolesConfigured: false,
      oidcConfigured: false,
      oidcLinkHtml: 'Sign in with OpenID',
      showServiceTokensUI: true,
    },
    version: '7.1.3',
  };

  const { config, currentUser, version } = data || _data;

  if (!config) {
    return {};
  }

  baseUrl(config.baseUrl);

  return { config, currentUser, version };
}

export default useAppContext;
