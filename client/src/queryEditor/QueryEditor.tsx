import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import SplitPane from 'react-split-pane';
import AppHeader from '../app-header/AppHeader';
import { debouncedResizeChart } from '../common/tauChartRef';
import SchemaInfoLoader from '../schema/SchemaInfoLoader';
import { connectConnectionClient, loadQuery } from '../stores/editor-actions';
import useShortcuts from '../utilities/use-shortcuts';
import DocumentTitle from './DocumentTitle';
import EditorNavProtection from './EditorNavProtection';
import EditorPaneRightSidebar from './EditorPaneRightSidebar';
import EditorPaneSchemaSidebar from './EditorPaneSchemaSidebar';
import EditorPaneVis from './EditorPaneVis';
import NotFoundModal from './NotFoundModal';
import QueryEditorResultPane from './QueryEditorResultPane';
import QueryEditorSqlEditor from './QueryEditorSqlEditor';
import QuerySaveModal from './QuerySaveModal';
import Toolbar from './Toolbar';
import UnsavedQuerySelector from './UnsavedQuerySelector';

interface Params {
  queryId?: string;
}

function QueryEditor() {
  const [showNotFound, setShowNotFound] = useState(false);
  const { queryId = '' } = useParams<Params>();
  console.log(
    '🚀 ~ 当 queryId 从 URL 字符串更改时，根据需要加载查询 QueryEditor ~ queryId:',
    queryId
  );
  useShortcuts();

  //当 queryId 从 URL 字符串更改时，根据需要加载查询。
  //如果 queryId 不存在，则是因为路由正在命中“/querys/new”，从而避免发送 queryId 参数
  //在新查询的情况下，状态已设置。
  //任何一个用户都是新登录的（默认情况下会设置新查询）
  //或者他们点击了新的查询按钮，该按钮在点击时重置状态。
  //不需要在此处调用 resetNewQuery。
  //如果未找到查询，则显示未找到的模态以通知用户并提示启动新的查询。
  useEffect(() => {
    setShowNotFound(false);
    if (queryId === '') {
      connectConnectionClient();
    } else if (queryId) {
      loadQuery(queryId).then(({ error, data }) => {
        console.log('loadQuery data:', data);
        if (error || !data) {
          return setShowNotFound(true);
        }
        connectConnectionClient();
      });
    }
  }, [queryId]);

  return (
    <div
      style={{
        height: '100vh',
        width: '100%',
        display: 'flex',
        flexDirection: 'column',
      }}
    >
      <AppHeader />
      <Toolbar />
      {/* 查询内容区域 */}
      <div style={{ position: 'relative', flexGrow: 1 }}>
        <EditorPaneRightSidebar queryId={queryId}>
          <EditorPaneSchemaSidebar queryId={queryId}>
            {/* @ts-expect-error SplitPane types are off */}
            <SplitPane
              split="horizontal"
              minSize={100}
              defaultSize={'60%'}
              maxSize={-100}
              onChange={() => debouncedResizeChart(queryId)}
            ><EditorPaneVis queryId={queryId}>
               {/* 编辑器 区域 */}
                <QueryEditorSqlEditor />
                {/* 编辑器右侧 ChartToolbar 区域 */}
              </EditorPaneVis>
              {/* 查询结果，和下载区域 */}
              <QueryEditorResultPane />
            </SplitPane>
          </EditorPaneSchemaSidebar>
        </EditorPaneRightSidebar>
      </div>
      <UnsavedQuerySelector queryId={queryId} />
      <DocumentTitle queryId={queryId} />
      <SchemaInfoLoader />
      <QuerySaveModal />
      <NotFoundModal visible={showNotFound} queryId={queryId} />
      <EditorNavProtection />
    </div>
  );
}

export default QueryEditor;
