# Dependencies

SQLPad 是一个旧项目，由于特定原因，一些依赖项被保留在特定版本中。

可以通过执行以下操作来检索过时的依赖项列表：

```sh
# from root
cd server # or client
yarn outdated
```

## Outdated server dependencies (as of 6/18/2023)

ldapjs- 3.x 带来了很多变化和破损风险
mariadb- 3.0.2 与续集一起使用。3.1.2 没有。
node-fetch- 3.x 仅适用于 ESM
query-string- 8.x 仅适用于 ESM
rim-raf- 3.1.2 添加仅 ESM 的传递依赖
sql-formatter- 3.x 及以后的版本有性能回归和功能变化。
umzug- 3.x 有许多（不必要的）重大更改和额外的依赖项。

## Outdated client dependencies (as of 2/28/2022)

```
Package                  Current    Wanted    Latest  Why Outdated
-------------------------------------------------------------------------------------------------------------------------------
@types/node             14.18.12  14.18.12   17.0.21  Using node 14 at the moment
d3                        5.16.0    5.16.0     7.3.0  taucharts requires d3@5
history                   4.10.1    4.10.1     5.3.0  react-router-dom@5 requires history@4
react-router-dom           5.3.0     5.3.0     6.2.2  react-router-dom@6 does not support Prompt component (yet)
```
